package acme

import "net/http"

const RoutePath = "/.well-known/acme-challenge/"

func AddHandleFunc(m map[string]string) {
	http.HandleFunc(RoutePath, func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type'", "text/plain")

		key := r.RequestURI[len(RoutePath):]
		value, ok := m[key]
		if ok {
			w.WriteHeader(http.StatusOK)
			w.Write([]byte(value))
		} else {
			w.WriteHeader(http.StatusNotFound)
			w.Write([]byte(""))
		}
	})
}
